package main

import (
    "encoding/json"
    "encoding/csv"
    "io/ioutil"
    "log"
    "strings"
    "fmt"
    "os"
)

type Clubs struct {
    Clubs   map[string]Club `json:"clubs"`
}

type Club struct {
    Type    string   `json:"type"`
    Name    string   `json:"name"`
    Desc    string   `json:"desc"`
    Heads   []string `json:"heads"`
    Socials []Social `json:"socials"`
    When    []string `json:"when"`
    Where   []string `json:"where"`
    Staff   []string `json:"staff"`
}

func (c Club) Strings() []string {
    socials := ""
    for i, social := range c.Socials {
        socials += fmt.Sprintf("(%s) %s", social[0], social[1])
        if i != len(c.Socials)-1 {
            socials += ", "
        }
    }
    return []string{
        c.Type,
        c.Name,
        c.Desc,
        strings.Join(c.Heads, ", "),
        socials,
        strings.Join(c.When, ", "),
        strings.Join(c.Where, ", "),
        strings.Join(c.Staff, ", "),
    }
}

type Social [2]string

var columns = []string{
    "type",
    "name",
    "desc",
    "heads",
    "socials",
    "when",
    "where",
    "staff",
}

const path = "clubs.json"
const destPath = "clubs.csv"

func main() {
    clubs := Clubs{ Clubs: map[string]Club{} }

    // Read JSON File
    b, err := ioutil.ReadFile(path)
    if err != nil {
        log.Fatalf("while reading file: %s", err)
    }
    
    // Unmarshal JSON
    err = json.Unmarshal(b, &clubs)
    if err != nil {
        log.Fatalf("while unmarshalling json: %s", err)
    }
    
    // Create Destination CSV File
    f, err := os.Create(destPath)
    if err != nil {
        log.Fatalf("while creating file: %s", err)
    }

    // TODO: buffer csv data and write in one call using ioutil.WriteFile
    w := csv.NewWriter(f)
    w.Write(columns)
    for k, club := range clubs.Clubs {
        w.Write(club.Strings())
        if err = w.Error(); err != nil {
            log.Fatalf("[%v] while writing csv: %s", k, err)
        }
        log.Printf("[%v] done", k)
    }

    // Flush CSV
    w.Flush()
    if err = w.Error(); err != nil {
        log.Fatalf("while writing csv: %s", err)
    }
}
