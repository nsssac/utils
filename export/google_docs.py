import io
import sys
import google

file_id = sys.argv[-1]
request = drive_service.files().export_media(fileId=file_id,
                                             mimeType='application/html')
fh = io.BytesIO()
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print(f'Downloaded {status.progress() * 100}%')

print(downloader)
