package main

import (
	"fmt"
	"io"
	"strings"
    "os"
    "bufio"
    "flag"
    "io/ioutil"
	"golang.org/x/net/html"
    "path/filepath"
)

type toTextFunc func(string) string

func htmlToText(src string) string {
    re := ""
	tokenizer := html.NewTokenizer(strings.NewReader(src))
	for {
		tt := tokenizer.Next()
		t := tokenizer.Token()
		err := tokenizer.Err()
		if err == io.EOF {
			break
		}
		switch tt {
		case html.ErrorToken:
            continue
		case html.TextToken:
			re += fmt.Sprintf("%s ", strings.TrimSpace(t.Data))
		}
	}
    return re
}

func txtToText(src string) string {
    return src
}

var fileType string
var fileIn string
var fileOut string

func init() {
    flag.StringVar(&fileType, "type", "auto", "file type of file to convert")
    flag.StringVar(&fileOut, "out", "-", "output file")
    flag.StringVar(&fileIn, "in", "", "input file")
}

func main() {
    var err error
    flag.Parse()
    if fileType == "auto" {
        ext := filepath.Ext(fileIn)
        if len(ext) > 0 {
            fileType = ext[1:]
        }
    }
    var toText toTextFunc
    switch fileType {
    case "html":
        toText = htmlToText
    default:
        toText = txtToText
    }
    var out io.Writer
    if fileOut == "-" {
        out = os.Stdout
    } else {
        if out, err = os.Create(fileOut); err != nil {
            panic(err)
        }
    }
    writer := bufio.NewWriter(out)
    inStr, err := ioutil.ReadFile(fileIn)
    if err != nil {
        panic(err)
    }
    writer.WriteString(toText(string(inStr)))
}
